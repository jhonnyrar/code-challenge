Rails.application.routes.draw do
  resources :produtos
  resources :pedidos do
    resources :items
    resources :estados, only: [:create]
  end

  get 'pedidos/relatorios/ticket_medio/com_data_inicial/:data_inicial/com_data_final/:data_final', to: 'ticketmedio#show'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
