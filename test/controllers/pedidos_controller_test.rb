require 'test_helper'

class PedidosControllerTest < ActionDispatch::IntegrationTest
  test "listar todos os pedidos" do
    get pedidos_url

    assert_response :success

    assert_not_nil @response
  end

  test "listar pedido pelo id" do
    get pedido_url(298487098)

    assert_response :success

    assert_not_nil @response
  end

  test "criar pedido" do
    pedido = Pedido.new
    pedido.codigo = 11112222
    pedido.comprador = 'jhonny'
    pedido.data = '21/04/2019'
    pedido.frete = 10

    post pedidos_url, params: {pedido:pedido.as_json}

    assert_response :created
  end

  test "cancelar pedido" do
    id = 1069870105
    delete pedido_url(id)

    assert_response :success
  end

  test "atualizar dados do pedido" do
    pedido = Pedido.find(298487098)
    pedido.codigo += 1
    pedido.comprador = 'jhonny1'
    pedido.data = '20/04/2019'
    pedido.frete += 1

    patch pedido_url(pedido.id), params: {pedido:pedido.as_json}

    assert_response :success
  end

end
