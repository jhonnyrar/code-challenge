require 'test_helper'

class ItemsControllerTest < ActionDispatch::IntegrationTest
  test "listar todos os items de um pedido" do
    get pedido_items_url(651017645)

    assert_response :success

    assert_not_nil @response
  end

  test "listar item de um pedido" do
    get pedido_item_url(651017645,161889662)

    assert_response :success

    assert_not_nil @response
  end

  test "criar item do pedido" do
    item = Item.new
    item.preco = 1
    item.quantidade = 1
    item.pedido_id = 651017645
    item.produto_id = 386719835

    post pedido_items_url(item.pedido_id), params: {item:item.as_json}

    assert_response :created
  end

  test "remover item" do
    delete pedido_item_url(651017645,161889662)

    assert_response :success
  end

  test "atualizar dados do produto" do
    item = Item.where(pedido_id: 651017645,id: 161889662).first

    item.preco = 2
    item.quantidade = 2
    item.produto_id = 873815415

    patch pedido_item_url(651017645,161889662), params: {item:item.as_json}

    assert_response :success
    assert_not_nil @response
  end
end
