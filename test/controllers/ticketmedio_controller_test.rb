require 'test_helper'

class TicketmedioControllerTest < ActionDispatch::IntegrationTest
  test "lista o ticket medio dos pedidos" do

    get pedidos_path+'/relatorios/ticket_medio/com_data_inicial/2019-04-19/com_data_final/2019-04-22'

    assert_response :success

    assert_not_nil @response
  end

end
