require 'test_helper'

class ProdutoControllerTest < ActionDispatch::IntegrationTest
  test "listar todos os produtos" do
    get produtos_url

    assert_response :success

    assert_not_nil @response
  end

  test "listar produto pelo id" do
    get produto_url(386719835)

    assert_response :success

    assert_not_nil @response
  end

  test "criar produto" do
    produto = Produto.new
    produto.codigo = 123456
    produto.estoque = 10
    produto.preco = 1.99
    produto.descricao = 'descricao de teste'
    produto.nome = 'nome de teste'

    post produtos_url, params: {produto:produto.as_json}

    assert_response :created
    assert_not_nil @response
  end

  test "remover produto" do
    delete produto_url(166491436)

    assert_response :success
  end

  test "atualizar dados do produto" do
    produto = Produto.find(386719835)

    produto.preco += 1
    produto.codigo += 1
    produto.nome = produto.nome + '1'
    produto.descricao = produto.descricao + '1'
    produto.estoque += 1

    patch produto_url(produto.id), params: {produto:produto.as_json}

    assert_response :success
    assert_not_nil @response
  end

end
