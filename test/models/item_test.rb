require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  test "verifica quantidade disponivel do produto em estoque" do
    item = Item.find(1077589)
    item.quantidade = 5

    assert_equal(true, item.valid?)
  end

  test "verifica quantidade superior ao disponivel em estoque do produto" do
    item = Item.find(1077589)
    item.quantidade = 6

    assert_equal(false, item.valid?)
  end

  test "baixar todos os itens disponiveis em estoque" do
    item = Item.find(1077589)
    item.baixarestoqueproduto
    item.reload

    assert_equal(0,item.produto.estoque)
  end

end
