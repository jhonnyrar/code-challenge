require 'test_helper'

class PedidoTest < ActiveSupport::TestCase

  test "Aprova pedido" do
    pedido = Pedido.find(298487098)
    pedido.aprovar
    pedido.save
    pedido.reload

    assert_equal(true, pedido.aprovado?)
  end

  test "entrega pedido aprovado" do
    pedido = Pedido.find(298487098)
    pedido.aprovar
    pedido.save
    pedido.entregar
    pedido.save
    pedido.reload

    assert_equal(true, pedido.entregue?)
  end

  test "nao permite entregar pedido sem aprovacao" do
    pedido = Pedido.find(298487098)

    assert_raise AASM::InvalidTransition do
      pedido.entregar
    end
  end

  test "cancelar pedido" do
    pedido = Pedido.find(298487098)
    pedido.cancelar
    pedido.save
    pedido.reload

    assert_equal(true, pedido.cancelado?)
  end

  test "baixar itens do estoque" do
    pedido = Pedido.find(950609938)
    pedido.baixarestoqueprodutos
    pedido.reload

    assert_equal(0,pedido.items.first.produto.estoque)
    assert_equal(0,pedido.items.last.produto.estoque)
  end

  test "estoque insuficiente para atender pedido" do
    pedido = Pedido.find(263067803)

    assert_equal(false, pedido.valid?)
  end

  test "emissao do ticket medio" do
     assert_equal 211.26, Pedido.ticketmedio('2019-04-19','2019-04-22')
  end
end
