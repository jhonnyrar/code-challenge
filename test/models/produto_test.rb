require 'test_helper'

class ProdutoTest < ActiveSupport::TestCase
  test "verifica quantidade disponivel do produto em estoque" do
    produto = Produto.find_by_codigo(2505703621852)

    assert_equal(true, produto.estoquedisponivelsuficiente?(5))
  end

  test "verifica quantidade superior ao disponivel em estoque do produto" do
    produto = Produto.find_by_codigo(2505703621852)

    assert_equal(false, produto.estoquedisponivelsuficiente?(6))
  end

  test "baixar todos os itens disponiveis em estoque" do
    produto = Produto.find_by_codigo(2505703621852)
    produto.baixarestoque(5)
    produto.save

    produto = Produto.find_by_codigo(2505703621852)
    assert_equal(0,produto.estoque)
  end
end
