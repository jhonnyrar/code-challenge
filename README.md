# Loja

Loja é um sistema REST para simular o ciclo das vendas de uma pequena loja.

  - Resources da loja
      + produtos 
        + GET /produtos
        + GET /produtos/:id
        + POST /produtos        
        + PATCH /produtos/:id
        + PUT /produtos/:id
        + DELETE /produtos/:id
      + pedidos
        + GET /pedidos
        + GET /pedidos/:id
        + POST /pedidos
        + PATCH /pedidos/:id
        + PUT /pedidos/:id
        + DELETE /pedidos/:id        
      + pedido_items
        + GET /pedidos/:pedido_id/items
        + GET /pedidos/:pedido_id/items/:id
        + POST /pedidos/:pedido_id/items
        + PATCH /pedidos/:pedido_id/items/:id
        + PUT /pedidos/:pedido_id/items/:id
        + DELETE /pedidos/:pedido_id/items/:id
      + pedido_estados
        + POST /pedidos/:pedido_id/estados
      + pedidos_relatorios
        + GET /pedidos/relatorios/ticket_medio/com_data_inicial/:data_inicial/com_data_final/:data_final
      
     
### Instalação

O caminho mais fácil para instalar o pacote da loja é via github:

```sh
$ git clone git@bitbucket.org:jhonnyrar/code-challenge.git
$ cd loja
$ bundle install
$ rails db:migrate
$ rails db:seed
$ rails s
```

### Execução

O serviço estará rodando no endereço http://localhost:3000/

Exemplo: 
```sh
$ curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/produtos
```
