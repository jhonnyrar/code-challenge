10.times  do
  produto = Produto.create({
                               codigo: Faker::Code.ean,
                               descricao: Faker::Commerce.material,
                               nome: Faker::Commerce.product_name,
                               estoque: Faker::Number.between(1,1000),
                               preco: Faker::Commerce.price
                           })

  3.times do
    pedido = Pedido.create( {
                                codigo: Faker::Number.number(10),
                                data: Faker::Date.backward(14),
                                comprador: Faker::FunnyName.name,
                                estado: :novo,
                                frete: Faker::Commerce.price
                            })

    Item.create({
        pedido_id: pedido.id,
        produto_id: produto.id,
        quantidade: Faker::Number.between(1,10),
        preco: produto.preco*1.30
                })
  end
end