class AddTotalToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :total, :float
  end
end
