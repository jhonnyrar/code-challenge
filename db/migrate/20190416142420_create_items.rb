class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.decimal :quantidade
      t.decimal :preco
      t.references :pedido, foreign_key: true
      t.references :produto, foreign_key: true

      t.timestamps
    end
  end
end
