class CreateProdutos < ActiveRecord::Migration[5.2]
  def change
    create_table :produtos do |t|
      t.integer :codigo
      t.string :nome
      t.string :descricao
      t.decimal :estoque
      t.decimal :preco

      t.timestamps
    end
  end
end
