class CreatePedidos < ActiveRecord::Migration[5.2]
  def change
    create_table :pedidos do |t|
      t.integer :codigo
      t.date :data
      t.string :comprador
      t.integer :estado
      t.decimal :frete

      t.timestamps
    end
  end
end
