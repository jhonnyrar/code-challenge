class EstadosController < ApplicationController
  def create
    @pedido = Pedido.find(params[:pedido_id])

    if @pedido.send(params[:acao])
      render json: @pedido
    else
      render json: @pedido.errors, status: :unprocessable_entity
    end
  end

  private
  def estado_params
    params.require(:acao)
  end
end
