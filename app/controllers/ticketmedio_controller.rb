class TicketmedioController < ApplicationController
  # 'pedidos/relatorios/ticket_medio/com_data_inicial/21-04-2019/com_data_final/21-04-2019'
  def show
    render json: {ticketmedio:Pedido.ticketmedio(params[:data_inicial],params[:data_final]).as_json}
  end
end
