class Pedido < ApplicationRecord
  include AASM
  has_many :items, dependent: :destroy

  validates_associated :items
  validates :codigo, presence: true, uniqueness: true
  validates :data, presence: true
  validates :comprador, presence: true
  validates :estado, presence: true
  validates :frete, presence: true, numericality: true

  enum estado: [
      :novo,
      :aprovado,
      :entregue,
      :cancelado
  ]

  aasm column: :estado, enum: true do
    state :novo, initial: true
    state :aprovado
    state :entregue
    state :cancelado

    event :aprovar do
      after do
        baixarestoqueprodutos
      end
      transitions from: :novo, to: :aprovado
    end

    event :entregar do
      transitions from: :aprovado, to: :entregue
    end

    event :cancelar do
      transitions from: [:novo,:aprovado], to: :cancelado
    end
  end

  def baixarestoqueprodutos
    items.find_each do |item|
      item.baixarestoqueproduto
    end
  end

  def self.ticketmedio(data_inicial, data_final)
    totaldospedidos = Pedido
                          .where(:data => data_inicial..data_final)
                          .joins(:items)
                          .sum(:total)

    quantidadepedidos = Pedido
                            .where(:data => data_inicial..data_final)
                            .count

    valormediodospedidos = (quantidadepedidos > 0) ? totaldospedidos/quantidadepedidos : 0
  end
end
