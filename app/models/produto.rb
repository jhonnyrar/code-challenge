class Produto < ApplicationRecord
  has_many :items

  validates :codigo, presence: true, uniqueness: true
  validates :nome, presence: true
  validates :descricao, presence: true
  validates :estoque, presence: true, numericality: true
  validates :preco, presence: true, numericality: true

  def baixarestoque(quantidade)
    self.estoque = self.estoque - quantidade
  end

  def estoquedisponivelsuficiente?(quantidade)
    self.estoque >= quantidade
  end
end
