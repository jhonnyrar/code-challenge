class Item < ApplicationRecord
  belongs_to :pedido
  belongs_to :produto

  validate :estoquedisponivelsuficiente
  validates :quantidade, presence: true, numericality: true
  validates :preco, presence: true, numericality: true

  before_save :calcularvalortotal

  def baixarestoqueproduto
    produto.baixarestoque(self.quantidade)
    produto.save
  end

  def estoquedisponivelsuficiente
    if (not self.produto.nil?) and (not self.produto.estoquedisponivelsuficiente?(self.quantidade))
      errors.add("quantidade","estoque insuficiente para o item")
    end
  end

  private
  def calcularvalortotal
    self.total = self.quantidade * self.preco
  end
end
